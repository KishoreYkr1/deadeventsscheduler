﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DeadEventsDailyCron.DistributorDBContext
{
    public partial class DBEmailDistributor_Entities : DbContext
    {
        public DBEmailDistributor_Entities()
        {
        }

        public DBEmailDistributor_Entities(DbContextOptions<DBEmailDistributor_Entities> options)
            : base(options)
        {
        }

        public virtual DbSet<EmailVendorAccess> EmailVendorAccesses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=tcp:dt-email-use-asql.database.windows.net,1433;Initial Catalog=EmailDistributor_PDB;Persist Security Info=False;User ID=web_app_user;Password=te876!nm9;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<EmailVendorAccess>(entity =>
            {
                entity.ToTable("EmailVendorAccess");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasComment("PK, Unique id for external email vendor connection");

                entity.Property(e => e.ApiprivateKey)
                    .HasMaxLength(300)
                    .HasColumnName("APIPrivateKey")
                    .HasComment("Private API access key information received from external vendor");

                entity.Property(e => e.ApipublicKey)
                    .HasMaxLength(300)
                    .HasColumnName("APIPublicKey")
                    .HasComment("Public API access key information received from external vendor");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasComment("Record create timestamp");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasComment("Custom description of connection information. Ex. Prod - Transaction Account, Prod - Marketing Account");

                entity.Property(e => e.DistributionRatio).HasComment("Email distribution percentage ratio. i.e. 50% or 30%");

                entity.Property(e => e.EmailVendorId)
                    .HasColumnName("EmailVendor_ID")
                    .HasComment("FK, External email vendor id - Reference from EmailVendor table");

                entity.Property(e => e.HealthCheckTimeStamp)
                    .HasColumnType("datetime")
                    .HasComment("Last healthcheck retrieve timestamp");

                entity.Property(e => e.IsActive).HasComment("Flag to check if the vendor access connection info is active");

                entity.Property(e => e.IsHealthy).HasComment("Flag to track if the vendor access connection is healthy");

                entity.Property(e => e.IsMarketingAccount).HasComment("Flag to track if is's Marketing Account");

                entity.Property(e => e.MasterLookupSysEnvironmentId)
                    .HasColumnName("MasterLookup_SysEnvironment_ID")
                    .HasComment("FK,MasterLookup id for lookuptype \"SysEnvironment\" - Reference from MasterLookup table. Used if running multiple env in one deployment. Ex: Test, Dev, QA etc");

                entity.Property(e => e.ResponseWebHookUrl)
                    .HasMaxLength(250)
                    .HasColumnName("ResponseWebHookURL")
                    .HasComment("Internal email notification event webhook address provided to external vendor");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
