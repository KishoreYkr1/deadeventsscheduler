﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DeadEventsDailyCron.GuidepointDBContext
{
    public partial class BouncedEmail
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public string Created { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? EmailVendorAccessId { get; set; }
        public string EmailVendorAccessDescription { get; set; }
    }
}
