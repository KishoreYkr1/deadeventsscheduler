using DeadEventsDailyCron.DistributorDBContext;
using DeadEventsDailyCron.GuidepointDBContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeadEventsDailyCron
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DBEmailDistributor_Entities>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Distributor_ConnString"), option => option.EnableRetryOnFailure());
                System.Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss.fff")}: Setting DB connection string value using Configuration.GetConnectionString(\"Distributor_ConnString\"), value {Configuration.GetConnectionString("CRM_ConnString")}");
            }, ServiceLifetime.Singleton);
            services.AddDbContext<DBGuidepoint_Entities>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("CRM_ConnString"), option => option.EnableRetryOnFailure());
                System.Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss.fff")}: Setting DB connection string value using Configuration.GetConnectionString(\"CRM_ConnString\"), value {Configuration.GetConnectionString("CRM_ConnString")}");
            }, ServiceLifetime.Singleton);
            services.AddHostedService<SchedulerHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Blocked and Bounced emails cron started.");
                });
            });
        }
    }
}
