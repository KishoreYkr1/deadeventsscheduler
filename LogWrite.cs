﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeadEventsDailyCron
{
    public class LogWrite
    {
        public string LogFileName { get; }

        public LogWrite(string _logFileName)
        {
            LogFileName = _logFileName;
        }


        public async Task AppendLog(string LogMessage, string LogType = "Info")
        {
            var msg = $"\n{LogType}: {DateTime.Now.ToShortDateString()} {DateTime.Now.ToLongTimeString()}: {LogMessage}";

            System.Console.WriteLine(msg);
            await File.AppendAllTextAsync(LogFileName, msg);
        }
    }
}
