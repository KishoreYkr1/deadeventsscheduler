﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DeadEventsDailyCron.DistributorDBContext
{
    public partial class EmailVendorAccess
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ApipublicKey { get; set; }
        public string ApiprivateKey { get; set; }
        public string ResponseWebHookUrl { get; set; }
        public int? DistributionRatio { get; set; }
        public bool? IsMarketingAccount { get; set; }
        public int? MasterLookupSysEnvironmentId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsHealthy { get; set; }
        public DateTime? HealthCheckTimeStamp { get; set; }
        public int? EmailVendorId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
