﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeadEventsDailyCron
{
    public class DataModel
    {
        public string email { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string created { get; set; }
    }
}
