﻿using DeadEventsDailyCron.DistributorDBContext;
using DeadEventsDailyCron.GuidepointDBContext;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace DeadEventsDailyCron
{
    public class SchedulerHostedService : IHostedService
    {
        private readonly  DBGuidepoint_Entities CRMDbContext;
        private readonly DBEmailDistributor_Entities DBEmailDistributorContext;
        private readonly ILogger<SchedulerHostedService> Logger;
        public IConfiguration Configuration { get; }
        public SchedulerHostedService(DBGuidepoint_Entities _crmDbContext, DBEmailDistributor_Entities _dBEmailDistributorContext,
            IConfiguration configuration, ILogger<SchedulerHostedService> _logger) {
            CRMDbContext = _crmDbContext;
            DBEmailDistributorContext = _dBEmailDistributorContext;
            Configuration = configuration;
            Logger = _logger;
        }
        public Timer timer;       
        public Task StartAsync(CancellationToken cancellationToken)
        {
            // timer repeates call to APICallToDeadEvents every 24 hours.
            timer = new Timer(
                  APICallToDeadEvents,
                  null,
                  TimeSpan.Zero,
                  TimeSpan.FromMinutes(1)
              );
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        private void APICallToDeadEvents(object state)
        {
            if (DateTime.Now.Hour != 1 && DateTime.Now.Minute != 0)
            {
                Logger.LogInformation($"API call started at {DateTime.Now.ToLongDateString()}");
                var VendorsList = (from a in DBEmailDistributorContext.EmailVendorAccesses where a.IsActive == true && a.IsHealthy == true select a).ToList();
                CallToBlockCalls(VendorsList);
                CallToBounceCalls(VendorsList);              
            }
        }
        private async void CallToBlockCalls(List<EmailVendorAccess> emailVendorAccesses)
        {
            try
            {
                using (HttpClient HttpClient = new HttpClient())
                {
                    HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    DateTime foo = DateTime.Now.Date.AddDays(-1);
                    long startDate = ((DateTimeOffset)foo).ToUnixTimeSeconds();
                    List<BlockedEmail> BlockedEmailList = new List<BlockedEmail>();
                    foreach (var item in emailVendorAccesses)
                    {
                        HttpClient.DefaultRequestHeaders.Authorization = null;
                        HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", item.ApiprivateKey);
                        var response = await HttpClient.GetAsync($"{Configuration.GetSection("BlockedEmailsAPI").Value}start_time={startDate}"); //?start_time={startDate}
                        if (response.IsSuccessStatusCode)
                        {
                            List<DataModel> result = JsonConvert.DeserializeObject<List<DataModel>>(response.Content.ReadAsStringAsync().Result);
                            foreach (var item1 in result)
                            {
                                BlockedEmail blockedEmail = new BlockedEmail()
                                {
                                    Email = item1.email,
                                    Reason = item1.reason,
                                    Status = item1.status,
                                    Created = item1.created,
                                    EmailVendorAccessId = item.Id,
                                    EmailVendorAccessDescription = item.Description,
                                    CreatedDate = DateTime.Now
                                };
                                BlockedEmailList.Add(blockedEmail);
                            }
                        }
                    }
                    Logger.LogInformation($"Total blocked email records - {BlockedEmailList.Count}");
                    if (BlockedEmailList.Count > 0)
                    {
                        await CRMDbContext.BlockedEmails.AddRangeAsync(BlockedEmailList);
                        await CRMDbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(JsonConvert.SerializeObject(ex));
            }            
        }
        private async void CallToBounceCalls(List<EmailVendorAccess> emailVendorAccesses)
        {
            try
            {
                using (HttpClient HttpClient = new HttpClient())
                {
                    HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    DateTime foo = DateTime.Now.Date.AddDays(-1);
                    long startDate = ((DateTimeOffset)foo).ToUnixTimeSeconds();
                    List<BouncedEmail> BouncedEmailList = new List<BouncedEmail>();
                    foreach (var item in emailVendorAccesses)
                    {
                        HttpClient.DefaultRequestHeaders.Authorization = null;
                        HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", item.ApiprivateKey);
                        var response = await HttpClient.GetAsync($"{Configuration.GetSection("BouncedEmailsAPI").Value}?start_time={startDate}");
                        if (response.IsSuccessStatusCode)
                        {
                            List<DataModel> result = JsonConvert.DeserializeObject<List<DataModel>>(response.Content.ReadAsStringAsync().Result);
                            foreach (var item1 in result)
                            {
                                BouncedEmail bouncedEmail = new BouncedEmail()
                                {
                                    Email = item1.email,
                                    Reason = item1.reason,
                                    Status = item1.status,
                                    Created = item1.created,
                                    EmailVendorAccessId = item.Id,
                                    EmailVendorAccessDescription = item.Description,
                                    CreatedDate = DateTime.Now
                                };
                                BouncedEmailList.Add(bouncedEmail);
                            }
                        }
                    }
                    Logger.LogInformation($"Total bounced email records - {BouncedEmailList.Count}");
                    if (BouncedEmailList.Count > 0)
                    {
                        await CRMDbContext.BouncedEmails.AddRangeAsync(BouncedEmailList);
                        await CRMDbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(JsonConvert.SerializeObject(ex));
            }
           
        }
    }
}
